
'''
Cythonized code where, as much as possible, static typing used to optimize performance.

Used practices:

- adding types
- efficient indexing with memoryviews (typing ndarray contents)

mainly used this as a guideline:

https://cython.readthedocs.io/en/latest/src/userguide/numpy_tutorial.html
'''

import numpy as np
cimport numpy as np
DTYPE = np.float
ctypedef np.float_t DTYPE_t
import pandas as pd


cdef class SES:
    
    cdef np.ndarray endog
    cdef float alpha
    
    def __init__(self, np.ndarray[DTYPE_t, ndim=1] endog):
        '''
        constructor:

        endog: numpy.ndarray of floats: endogeneous input data series
        '''
        self.endog = endog
    
    cdef DTYPE_t get_level(self, DTYPE_t y_alpha, DTYPE_t alphac, DTYPE_t l):
        '''
        calculates the level at time t:

        y_alpha: alpha*y(t): variable of type np.float_t
        alphac: (1-alpha): variable of type int
        l: level(t-1): variable of type np.float_t

        '''
        return y_alpha + alphac*l

    cdef DTYPE_t get_forecast(self, DTYPE_t l, int h):
        '''
        calculates the forecast at time t+h (very simple, for expansion reasons later)

        l: level(t): variable of type np.float_t
        h: timepoint for the forecast (t+h): variable of type int

        '''
        return l 

    def fit(self, float a=0.2, int h=-1):
        """
        Fit the model

        Parameters
        ----------
        a : float, optional
            The alpha value of the simple exponential smoothing, if the value
            is set then this value will be used as the value.
        
        Returns
        -------
        results : HoltWintersResults class
            See statsmodels.tsa.holtwinters.HoltWintersResults

        """
        cdef float alpha = a
        cdef np.ndarray y = self.endog
        cdef int nobs = len(y)
        cdef np.ndarray[DTYPE_t, ndim=1] y_alpha = np.zeros((nobs,),dtype=DTYPE)
        cdef DTYPE_t alphac = 1 - alpha
        cdef np.ndarray[DTYPE_t, ndim=1] lvls = np.zeros((nobs + h + 2,),dtype=DTYPE)
        cdef np.ndarray[DTYPE_t, ndim=1] resid = np.zeros((nobs,),dtype=DTYPE)
        
        y_alpha[:] = alpha * y
        lvls[0] = y[0]
        
        for i in range(1, nobs + 1):
            lvls[i] = self.get_level(y_alpha[i-1], alphac, lvls[i-1])

        for i in range(0,h):
            lvls[nobs+i] = self.get_forecast(lvls[nobs],i)
#         resid = y - lvls[:-h - 1]

        return {'fittedvalues':lvls[:]}