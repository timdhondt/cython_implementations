from distutils.core import setup
from Cython.Build import cythonize
import numpy

setup(name='Single Exponential Smoothing', author='Tim dHondt',
      ext_modules=cythonize("single_exp_smoothing.pyx"), include_dirs=[numpy.get_include()])