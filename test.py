from single_exp_smoothing import SES
import pandas as pd
import matplotlib.pyplot as plt

# load dataset into pandas
loc = r'C:\Users\timdhondt\Documents\Tim\datasets\timeseries_generated.xlsx'
data = pd.read_excel(loc)
fullseries = data['quantity']
# create subset of data (note the .values)
subset = fullseries.head(60).values

# fit the model
fit = SES(subset).fit(0.2)

plt.plot(subset, color='black')
plt.plot(fit['fittedvalues'])
plt.show()